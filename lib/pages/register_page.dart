import 'package:chat_app/witgets/button.dart';
import 'package:chat_app/witgets/custom_input.dart';
import 'package:chat_app/witgets/labels.dart';
import 'package:chat_app/witgets/logo.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Logo(
                title: 'Registro',
              ),
              _FormState(),
              Labels(
                path: 'login',
                title: 'Ya tienes una cuenta?',
                subTitle: 'Ingresa',
              ),
              Text(
                'Terminos y condiciones de uso',
                style: TextStyle(fontWeight: FontWeight.w200),
              )
            ],
          ),
        ),
      )),
    );
  }
}

class _FormState extends StatefulWidget {
  @override
  __FormStateState createState() => __FormStateState();
}

class __FormStateState extends State<_FormState> {
  final nameCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 40),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: [
          CustomInput(
            icon: Icons.perm_identity,
            placeholder: 'Nombre',
            keyboardType: TextInputType.emailAddress,
            textControler: nameCtrl,
          ),
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textControler: emailCtrl,
          ),
          CustomInput(
            icon: Icons.vpn_key_outlined,
            placeholder: 'Contraseña',
            textControler: passwordCtrl,
            isPassword: true,
          ),
          Button(
              color: Colors.blue,
              label: 'Ingresar',
              calback: () {
                print(emailCtrl.text);
                print(passwordCtrl.text);
                print(nameCtrl.text);
              })
        ],
      ),
    );
  }
}
