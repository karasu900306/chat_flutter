import 'package:flutter/material.dart';

class ChatMessage extends StatefulWidget {
  final String texto;
  final String uid;
  final AnimationController animationController;

  const ChatMessage(
      {Key key,
      @required this.texto,
      @required this.uid,
      @required this.animationController})
      : super(key: key);

  @override
  _ChatMessageState createState() => _ChatMessageState();
}

class _ChatMessageState extends State<ChatMessage> {
  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: this.widget.animationController,
      child: SizeTransition(
        sizeFactor: CurvedAnimation(
            parent: this.widget.animationController, curve: Curves.easeIn),
        child: Container(
          child: Container(
            child: this.widget.uid == '123' ? _myMessage() : _notMymessage(),
          ),
        ),
      ),
    );
  }

  _myMessage() {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        padding: EdgeInsets.all(8.0),
        margin: EdgeInsets.only(right: 10, bottom: 5, left: 50),
        child: Text(
          this.widget.texto,
          style: TextStyle(color: Colors.white),
        ),
        decoration: BoxDecoration(
            color: Color(0xff4D9EF6), borderRadius: BorderRadius.circular(20)),
      ),
    );
  }

  _notMymessage() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.all(8.0),
        margin: EdgeInsets.only(left: 10, bottom: 5, right: 50),
        child: Text(
          this.widget.texto,
          style: TextStyle(color: Colors.black87),
        ),
        decoration: BoxDecoration(
            color: Color(0xffE4E5E8), borderRadius: BorderRadius.circular(20)),
      ),
    );
  }
}
