import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final Color color;
  final Function calback;
  final String label;

  const Button(
      {Key key,
      @required this.color,
      @required this.calback,
      @required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        elevation: 2,
        highlightElevation: 5,
        color: this.color,
        shape: StadiumBorder(),
        onPressed: this.calback,
        child: Container(
          width: double.infinity,
          height: 55,
          child: Center(
            child: Text(this.label,
                style: TextStyle(color: Colors.white, fontSize: 17)),
          ),
        ),
      ),
    );
  }
}
